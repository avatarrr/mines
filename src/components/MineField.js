import React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import Field from './Field';

export default class MineField extends React.Component {
  render = () => {
    const rows = this.props.board.map((row, rowIndex) => {
      const columns = row.map((field, columnIndex) => {
        return (
          <Field
            {...field}
            key={columnIndex}
            onOpen={() => this.props.onOpenField(rowIndex, columnIndex)}
            onFlag={() => this.props.onFlagField(rowIndex, columnIndex)}
          />
        );
      });

      return (
        <SafeAreaView style={styles.line} key={rowIndex}>
          {columns}
        </SafeAreaView>
      );
    });

    return <SafeAreaView style={styles.container}>{rows}</SafeAreaView>;
  };
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#EEE',
  },
  line: {
    flexDirection: 'row',
  },
});

import React from 'react';
import {
  StyleSheet,
  Text,
  SafeAreaView,
  TouchableWithoutFeedback,
} from 'react-native';
import params from '../params';
import Flag from './Flag';
import Mine from './Mine';

export default class Field extends React.Component {
  generateStylesForField = () => {
    const stylesForField = [styles.field];

    const {opened, exploded, flagged} = this.props;
    if (opened) {
      stylesForField.push(styles.opened);
    }

    if (exploded) {
      stylesForField.push(styles.exploded);
    }

    if (flagged) {
      stylesForField.push(styles.flagged);
    }

    if (!opened && !exploded) {
      stylesForField.push(styles.regular);
    }

    return stylesForField;
  };

  generateColorOfNumberOfNearMiner = () => {
    const {nearMines} = this.props;

    let color = null;

    if (nearMines > 0) {
      if (nearMines >= 1) {
        color = '#2A28D7';
      }
      if (nearMines >= 2) {
        color = '#2B520F';
      }
      if (nearMines > 2 && nearMines < 6) {
        color = '#F9060A';
      }
      if (nearMines >= 6) {
        color = '#F221A9';
      }
    }

    return color;
  };

  render = () => {
    const {opened, nearMines, mined, flagged} = this.props;

    return (
      <TouchableWithoutFeedback
        onPress={this.props.onOpen}
        onLongPress={this.props.onFlag}>
        <SafeAreaView style={this.generateStylesForField()}>
          {!mined && opened && nearMines > 0 ? (
            <Text
              style={[
                styles.label,
                {color: this.generateColorOfNumberOfNearMiner()},
              ]}>
              {nearMines}
            </Text>
          ) : (
            false
          )}
          {mined && opened ? <Mine /> : false}
          {flagged && !opened ? <Flag /> : false}
        </SafeAreaView>
      </TouchableWithoutFeedback>
    );
  };
}

const styles = StyleSheet.create({
  field: {
    height: params.blockSize,
    width: params.blockSize,
    borderWidth: params.borderSize,
  },
  regular: {
    backgroundColor: '#999',
    borderLeftColor: '#CCC',
    borderTopColor: '#CCC',
    borderRightColor: '#333',
    borderBottomColor: '#333',
  },
  opened: {
    backgroundColor: '#999',
    borderColor: '#777',
    alignItems: 'center',
    justifyContent: 'center',
  },
  exploded: {
    backgroundColor: 'red',
    borderColor: 'red',
  },
  flagged: {},
  label: {
    fontWeight: 'bold',
    fontSize: params.fontSize,
  },
});

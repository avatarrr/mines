import React from 'react';
import {
  Modal,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';

export default class LevelSelection extends React.Component {
  render = () => {
    return (
      <Modal
        onRequestClose={this.props.onCancel}
        visible={this.props.isVisible}
        animationType="slide"
        transparent={true}>
        <SafeAreaView style={styles.frame}>
          <SafeAreaView style={styles.container}>
            <Text style={styles.title}>Select the level!</Text>
            <TouchableOpacity
              style={[styles.button, styles.bgEasy]}
              onPress={() => this.props.onLevelSelected(0.1)}>
              <Text style={styles.buttonLabel}>Easy</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.button, styles.bgNormal]}
              onPress={() => this.props.onLevelSelected(0.2)}>
              <Text style={styles.buttonLabel}>Normal</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.button, styles.bgHard]}
              onPress={() => this.props.onLevelSelected(0.3)}>
              <Text style={styles.buttonLabel}>Hard</Text>
            </TouchableOpacity>
          </SafeAreaView>
        </SafeAreaView>
      </Modal>
    );
  };
}

const styles = StyleSheet.create({
  frame: {
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.6)',
  },
  container: {
    backgroundColor: '#EEE',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 15,
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    color: 'grey',
  },
  button: {
    marginTop: 10,
    padding: 5,
  },
  buttonLabel: {
    fontSize: 20,
    color: '#EEE',
  },
  bgEasy: {
    backgroundColor: '#49b65d',
  },
  bgNormal: {
    backgroundColor: '#2765f7',
  },
  bgHard: {
    backgroundColor: '#f26337',
  },
});

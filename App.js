import React from 'react';
import {Alert, SafeAreaView, StyleSheet, Text} from 'react-native';
import Header from './src/components/Header';
import MineField from './src/components/MineField';
import {
  cloneBoard,
  createMineBoard,
  flagsUsed,
  hadExplosion,
  invertFlag,
  openField,
  showMines,
  wonGame,
} from './src/functions';
import params from './src/params';
import LevelSelection from './src/screens/LevelSelection';

export default class Mines extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.createState();
  }

  minesAmount = () => {
    const columns = params.getColumnsAmount();
    const rows = params.getRowsAmount();

    return Math.ceil(columns * rows * params.difficultLevel);
  };

  createState = () => {
    const columns = params.getColumnsAmount();
    const rows = params.getRowsAmount();

    return {
      board: createMineBoard(rows, columns, this.minesAmount()),
      won: false,
      lost: false,
      showLevelSelection: false,
    };
  };

  wonMessage = () => Alert.alert('You won!', 'UIIIIIIIIIIIIII');

  onOpenField = (row, column) => {
    const board = cloneBoard(this.state.board);
    openField(board, row, column);

    const lost = hadExplosion(board);
    const won = wonGame(board);

    if (lost) {
      showMines(board);
      Alert.alert('Lost!', 'KKKKKKKKKKKKKKKKK');
    }

    if (won) {
      this.wonMessage();
    }

    this.setState({board, lost, won});
  };

  onFlagField = (row, column) => {
    const board = cloneBoard(this.state.board);
    invertFlag(board, row, column);

    const won = wonGame(board);

    if (won) {
      this.wonMessage();
    }

    this.setState({board, won});
  };

  onLevelSelected = level => {
    params.difficultLevel = level;
    this.setState(this.createState());
  };

  render = () => {
    return (
      <SafeAreaView style={styles.container}>
        <LevelSelection
          isVisible={this.state.showLevelSelection}
          onLevelSelected={this.onLevelSelected}
          onCancel={() => this.setState({showLevelSelection: false})}
        />
        <Header
          flagsLeft={this.minesAmount() - flagsUsed(this.state.board)}
          onNewGame={() => this.setState(this.createState())}
          onFlagPress={() => this.setState({showLevelSelection: true})}
        />
        <SafeAreaView style={styles.board}>
          <MineField
            board={this.state.board}
            onOpenField={this.onOpenField}
            onFlagField={this.onFlagField}
          />
        </SafeAreaView>
      </SafeAreaView>
    );
  };
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: 'flex-end',
  },
  board: {
    alignItems: 'center',
    backgroundColor: '#AAA',
  },
});

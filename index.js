/**
 * @format
 */

import {AppRegistry} from 'react-native';
import Mines from './App';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Mines);
